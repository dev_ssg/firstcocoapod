//
//  ViewController.swift
//  FirstCocoapod
//
//  Created by Selim on 09/15/2019.
//  Copyright (c) 2019 Selim. All rights reserved.
//

import UIKit

// Step 1: Dont forget to import pod!
import FirstCocoapod

class ViewController: UIViewController {

    // Step 2: Declare or connect an image view. Be sure to set the width and height constraints to the same value.
    @IBOutlet weak var testImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Step 3: Call the roundViewWith method on your image view.
        testImageView.roundViewWith(borderColor: UIColor.white, borderWidth: 5.0)
    }

}

