<p align="center">
<img width="460" height="300" src="https://bitbucket.org/dev_ssg/firstcocoapod/raw/93304ffed4e8961778651bdc6ba69bdd41489fe4/Example/FirstCocoapod/Images.xcassets/pexels-photo-374710.imageset/pexels-photo-374710.jpg">
</p>

# FirstCocoapod

[![CI Status](https://img.shields.io/travis/Selim/FirstCocoapod.svg?style=flat)](https://travis-ci.org/Selim/FirstCocoapod)
[![Version](https://img.shields.io/cocoapods/v/FirstCocoapod.svg?style=flat)](https://cocoapods.org/pods/FirstCocoapod)
[![License](https://img.shields.io/cocoapods/l/FirstCocoapod.svg?style=flat)](https://cocoapods.org/pods/FirstCocoapod)
[![Platform](https://img.shields.io/cocoapods/p/FirstCocoapod.svg?style=flat)](https://cocoapods.org/pods/FirstCocoapod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FirstCocoapod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FirstCocoapod'
```

## Author

Selim, sajkfiasjfajfadf

## License

FirstCocoapod is available under the MIT license. See the LICENSE file for more info.
